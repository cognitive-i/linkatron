<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Link-a-tron Bookmarklet</title>
</head>
<body>

<pre>

<%@ page import="java.net.URLEncoder, java.nio.charset.StandardCharsets" %>
<%
	String title = "Share VoD";
	
	// needs to be absolute path for bookmarklet
	String baseUrl = request.getRequestURL().toString()
			.replace(request.getRequestURI(), request.getContextPath());
	
	String bookmarkletTarget = baseUrl + "/sharelink/share";

	String shareScript =
			("(function () {" +
		 	"var t = 'sharer' + (new Date()).getTime(), " +
		    "d = document, l = location, e = encodeURIComponent, " +
		    "u = '{{TARGET}}?u='+ e(l.href) + '&t=' + e(d.title); " + 
		 	"var a = function() {" +
			"if(! window.open(u, t, 'toolbar=0,status=0,resizable=1,width=640,height=480'))" +
			"    l.href = u;" + 
		    "};" +
		    "setTimeout(a, 0);" +
			"}());").replace("{{TARGET}}", bookmarkletTarget);

	out.println(shareScript);

%>
</pre>
Drag this button to your bookmark bar:<br/>
<a href="javascript:<%= shareScript %>" title="<%= title %>" onclick="return false;"><img src="bookmarklet.png" alt="<%= title %>" border="0"/></a>

</body>
</html>