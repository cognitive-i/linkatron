package com.cognitivei.linkatron.resolvers;

import java.util.List;

public interface IMediaResolver {
	List<String> resolveMediaUrl(String url);
	String generateMediaLink(String url);

}
