package com.cognitivei.linkatron.resolvers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;

public class FakeResolver implements IMediaResolver {

	static Map<String, URI> mIdsUrls = new HashMap<String, URI>(1024);
		
	public List<String> resolveMediaUrl(String id) {
		URI asset = mIdsUrls.get(id);
		
		if(null != asset)
			return Arrays.asList(new String[]{asset.toString()});
		else
			return Arrays.asList(new String[]{});
	}

	public String generateMediaLink(String url) {
		try {
			URI asset = new URI(url);
			String id = DigestUtils.sha1Hex(url);
			mIdsUrls.put(id, asset);
			return id;
		}
		catch(URISyntaxException e)
		{
			return "XXXXX";
		}
	}
	
	

}
