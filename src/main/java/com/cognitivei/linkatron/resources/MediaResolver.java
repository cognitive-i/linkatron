package com.cognitivei.linkatron.resources;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.cognitivei.linkatron.resolvers.FakeResolver;
import com.cognitivei.linkatron.resolvers.IMediaResolver;
import com.google.common.base.Joiner;

@Path("/mediaresolver")
public class MediaResolver {

	@Inject
	IMediaResolver mResolver = new FakeResolver();
	
	@GET
	@Path("/{id}")
	public String getMediaLinks(@PathParam("id") String id) {
		return "[" + Joiner.on(",").join(mResolver.resolveMediaUrl(id)) + "]";
	}
	
}
