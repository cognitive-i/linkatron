package com.cognitivei.linkatron.resources;

import javax.inject.Inject;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import com.cognitivei.linkatron.emaildispatchers.IEmailDispatcher;
import com.cognitivei.linkatron.emaildispatchers.SimpleEmailDispatcher;
import com.cognitivei.linkatron.resolvers.FakeResolver;
import com.cognitivei.linkatron.resolvers.IMediaResolver;


@Path("/sharelink")
public class Bookmarklet {
	@Inject
	IMediaResolver mResolver = new FakeResolver();
	IEmailDispatcher mEmailDispatcher = new SimpleEmailDispatcher();
	
	@GET
	@Path("/share")
	public String promptForRecipents(@QueryParam("u") String url, @QueryParam("t") String title) {
		String id = mResolver.generateMediaLink(url);
		return "<html><body>" +
			"<form method='POST'>" +
			"<input type='text' name='i' value='" + id + "'/>" +
			"<input type='text' name='t' value='hashaday@gmail.com'/>" +
			"<input type='text' name='f' value='Fat Bob'/>" +
			"<input type='submit' value='send'/>" +
			"</form>" +
			"</body> </html>";
	}

	@POST
	@Path("/share")
	public String enqueueLink(@FormParam("i") String id, @FormParam("t") String emailTo, @FormParam("f") String from, @Context UriInfo request) {

		String url = request.getBaseUri().toString() + "mediaresolver/" + id;
		mEmailDispatcher.enqueue(emailTo, from, url, "");
		
		return "<html>" + 
				"<head><script>setTimeout(window.close, 5000);</script></head>" + 
				"<body>Email enqueued to " + emailTo + "</body></html>";
	}
}
