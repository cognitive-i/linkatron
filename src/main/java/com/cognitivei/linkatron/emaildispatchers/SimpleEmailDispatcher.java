package com.cognitivei.linkatron.emaildispatchers;

public class SimpleEmailDispatcher implements IEmailDispatcher {

	public void enqueue(String to, String from, String assetLink, String message) {
		// constructor will spawn thread and that will keep
		// object alive for required duration
		// all work is delegated to thread to ensure fast
		// HTTP response
		new SimpleEmailDispatcherJob(to, from, assetLink, message);
	}
	
	

}
