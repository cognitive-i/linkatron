package com.cognitivei.linkatron.emaildispatchers;

public interface IEmailDispatcher {
	void enqueue(String to, String from, String assetLink, String message);
}
