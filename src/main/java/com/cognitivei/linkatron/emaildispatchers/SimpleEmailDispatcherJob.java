package com.cognitivei.linkatron.emaildispatchers;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

public class SimpleEmailDispatcherJob extends Thread {
	
	private String mTo;
	private String mFrom;
	private String mAssetLink;
	private String mMessage;

	public SimpleEmailDispatcherJob(String to, String from, String assetLink, String message) {
		mTo = "gomi@littleworld.co.uk";//to;
		mFrom = "linkatron@littleworld.co.uk"; //from;
		mAssetLink = assetLink;
		mMessage = message;
		start();
	}
	
	
	public void run() {
		try {	
			Email email = new SimpleEmail();
			email.setHostName("demeter.krystal.co.uk");
			email.setSmtpPort(465);
			email.setAuthenticator(new DefaultAuthenticator("linkatron@littleworld.co.uk", "b0jboom!"));
			email.setSSL(true);
	
			email.setFrom(mFrom);
			email.setSubject("Link-a-tron VOD link");
			email.setMsg(mMessage + "\n" + mAssetLink);
			email.addTo(mTo);
			email.send();
		} catch(EmailException e)
		{
			// catch and give up
		}
		
		
	}

}
