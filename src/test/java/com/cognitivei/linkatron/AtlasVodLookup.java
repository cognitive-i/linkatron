package com.cognitivei.linkatron;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class AtlasVodLookup {
	
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
				{"Atlas docs known good", AtlasConstants.ATLAS_DOCS_TEST_BBC_URI},
				{"iPlayer link (Eastenders)", "http://www.bbc.co.uk/iplayer/episode/b0612rs2"},
				{"iPlayer link resolved (Eastenders)", "http://www.bbc.co.uk/iplayer/episode/b0612rs2/eastenders-03072015"},
				{"iPlayer link programme (Eastenders Series)", "http://www.bbc.co.uk/programmes/b006m86d"},
				{"Amazon Prime Video (RED2)", "http://www.amazon.co.uk/gp/product/B00IKESSD4"},
				{"Demand 5 (Big Brother)", "http://www.channel5.com/shows/big-brother/episodes/episode-55-87"},
				{"Demand 4 (Amazing Sheds)", "http://www.channel4.com/programmes/amazing-spaces-shed-of-the-year/on-demand/60962-003"}
		});
	}
	
	public AtlasVodLookup(String description, String vodAsset) {
		mVodAssetDescription = description;
		mVodAssetUri = vodAsset;
	}

	private String mVodAssetDescription;
	private String mVodAssetUri;
	
	
	@Test
	public void validateLookup() throws IOException, URISyntaxException {
		List<NameValuePair> l = Arrays.asList(new NameValuePair[] {
				new BasicNameValuePair("uri", mVodAssetUri),
				new BasicNameValuePair("apiKey", AtlasConstants.API_KEY) });
		
		String query = encodeQuery(l);
		URI ep = new URI("https", AtlasConstants.HOST, "/3.0/content.json", query, null);
		
		Content c = Request.Get(ep)
			.execute().returnContent();
		
		JSONObject r = new JSONObject(c.asString());
		JSONArray contents = r.getJSONArray("contents");
		assertEquals(mVodAssetDescription, 1, contents.length());
		
		if(contents.length() >= 1)
		{
			 JSONObject firstMatch = contents.getJSONObject(0);
			 String title = firstMatch.getString("title");
			 System.out.println(mVodAssetUri + " => " + title);
		}
	}
	
	private String encodeQuery(List<NameValuePair> kv) {
		// this hack is required because Atlas v3 API doesn't
		// understand url encoding
		String query = "";	
		Iterator<NameValuePair> i = kv.iterator();
		while(i.hasNext())
		{
			NameValuePair p = i.next();
			query += p.getName() + "=" + p.getValue() + "&";
		}
		
		if(0 < query.length())
			query = query.substring(0, query.length() -1);
		
		return query;
	}
}
