package com.cognitivei.linkatron;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

public class AtlasAcceptsUrlEncoding {
	
	@Test
	public void bbcProgrammeIdWithUrlEscaping() throws IOException, URISyntaxException {
		List<NameValuePair> l = Arrays.asList(new NameValuePair[] {
				new BasicNameValuePair("uri", AtlasConstants.ATLAS_DOCS_TEST_BBC_URI),
				new BasicNameValuePair("apiKey", AtlasConstants.API_KEY) });
		
		String query = URLEncodedUtils.format(l, StandardCharsets.UTF_8);
		URI ep = new URI("https", AtlasConstants.HOST, "/3.0/content.json", query, null);
		
		Content c = Request.Get(ep)
			.execute().returnContent();
		
		JSONObject r = new JSONObject(c.asString());
		JSONArray contents = r.getJSONArray("contents");
		assertEquals(1, contents.length());
	}	
	
	@Test
	public void bbcProgrammeIdWithHackedParameters() throws IOException, URISyntaxException {
		List<NameValuePair> l = Arrays.asList(new NameValuePair[] {
				new BasicNameValuePair("uri", AtlasConstants.ATLAS_DOCS_TEST_BBC_URI),
				new BasicNameValuePair("apiKey", AtlasConstants.API_KEY) });
		
		String query = encodeQuery(l);
		URI ep = new URI("https", AtlasConstants.HOST, "/3.0/content.json", query, null);
		
		Content c = Request.Get(ep)
			.execute().returnContent();
		
		JSONObject r = new JSONObject(c.asString());
		JSONArray contents = r.getJSONArray("contents");
		assertEquals(1, contents.length());
	}
	
	private String encodeQuery(List<NameValuePair> kv) {
		// this hack is required because Atlas v3 API doesn't
		// understand url encoding
		String query = "";	
		Iterator<NameValuePair> i = kv.iterator();
		while(i.hasNext())
		{
			NameValuePair p = i.next();
			query += p.getName() + "=" + p.getValue() + "&";
		}
		
		if(0 < query.length())
			query = query.substring(0, query.length() -1);
		
		return query;
	}	
}
